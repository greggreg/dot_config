{ config, pkgs, ... }:

{
  # Let Home Manager install and manage itself.
  programs.home-manager.enable = true;

  # Home Manager needs a bit of information about you and the
  # paths it should manage.
  home.username = "gbaraghimian";
  home.homeDirectory = "/Users/gbaraghimian";

  # This value determines the Home Manager release that your
  # configuration is compatible with. This helps avoid breakage
  # when a new Home Manager release introduces backwards
  # incompatible changes.
  #
  # You can update Home Manager without changing this value. See
  # the Home Manager release notes for a list of state version
  # changes in each release.
  home.stateVersion = "21.05";

  home.file = {
    ".tmux.conf".source = ../tmux/tmux.conf;
  };

  home.sessionVariables = {
    PKG_CONFIG_PATH = "${pkgs.openssl.dev}/lib/pkgconfig";
  };

  # Packages to install
  home.packages = [
    # Base packages probably for every computer
    pkgs.helix     # Editor
    pkgs.fd        # find replacement
    pkgs.fzf       # fuzzy file finder
    pkgs.tokei     # lines of code counter
    pkgs.zsh       # shell
    pkgs.bat       # cat replacement
    pkgs.tmux      # terminal multiplexer
    pkgs.ripgrep   # grep replacement
    pkgs.eza       # ls replacement

    # Javascript / Html development
    pkgs.nodePackages.prettier # javascript, json, css, html code formatter
    #pkgs.yarn

    # Random unix tools
    # pkgs.ncurses
    # pkgs.pkg-config
    # pkgs.openssl
    # pkgs.openssl_1_1
    # pkgs.rust-analyzer
    # pkgs.asciinema

  ];

  programs = {
    # direnv and bash.enable are for getting nix-direnv to work
    # https://github.com/nix-community/nix-direnv
    direnv = {
      enable = true;
      enableBashIntegration = true; # see note on other shells below
      nix-direnv.enable = true;
    };
    bash.enable = true; # see note on other shells below

    zsh = {
      enable = true;
      enableAutosuggestions = true;
      history.extended = true;
      dotDir = ".config/zsh/dotfiles";
      initExtra = ''
        # If asdf is installed via homebrew
        #   ASDF_DIR="$(brew --prefix asdf)/libexec"
        #   . "$ASDF_DIR/asdf.sh"

        # If asdf is installed via nix
        #. ${pkgs.asdf-vm}/share/asdf-vm/asdf.sh
      '';
      envExtra = ''
        #
        # Choose theme based on os-type
        #
        case "$OSTYPE" in
          darwin*)
            ZSH_THEME="amuse"
          ;;
          linux*)
            ZSH_THEME="dst"
          ;;
        esac
      '';
      loginExtra = ''
        source ~/.config/zsh/zshrc_dynamic
        setopt extendedglob
        bindkey '^R' history-incremental-pattern-search-backward
        bindkey '^F' history-incremental-pattern-search-forward
        export PATH=~/.config/bin:$PATH

        # For direnv
        eval "$(direnv hook zsh)"
              '';
    };
  };
}
