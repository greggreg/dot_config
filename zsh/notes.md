# Find and Replace folder wide

fd -e ext | xargs sed -i '' 's/Find/Replace/g'

# Postmaster pid on mac

/usr/local/var/postgres/postmaster.pid
