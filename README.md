# Environment Setup

* Install nix: https://nixos.org/guides/install-nix.html
* Install home-manager: https://github.com/nix-community/home-manager
* Make sure: `source $HOME/.nix-profile/etc/profile.d/nix.sh`
* clone this repo to `~/.config`
* edit `home.username` and `home.homeDirectory` in `home.nix`
* `home-manager switch`

# Setting up git for multiple accounts
create a `.gitconfig` file in the users project root
```
[core]
  SshCommand = "ssh -i ~/.ssh/ssh_key_for_work_account"
[user]
  name = name for account
  email = email@for_account.com
```

# Ubuntu mono font
 - grab latest release from: https://github.com/canonical/UbuntuMono-fonts/releases/
 - unzip and install the OTF font

## Mac Specific
* install homebrew: https://brew.sh/
* `brew install asdf postgresql wxmac coreutils pgp karabiner-elements the-unarchiver`


### Settings
* Turn key repeat to max
* Map caps-lock to control


## Linux
* change the hostname to something nicer
* add the kitty terminfo: https://sw.kovidgoyal.net/kitty/faq.html#i-get-errors-about-the-terminal-being-unknown-or-opening-the-terminal-failing-when-sshing-into-a-different-computer
  * `kitty +kitten ssh myserver`
* Add zsh to `etc/shells`
  *  `command -v zsh | sudo tee -a /etc/shells`
  *  `sudo chsh -s "$(command -v zsh)" "${USER}"`
* Chang the users default shell: `chsh -s $(which zsh)`
