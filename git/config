# Notes
# git checkout --conflict=diff3 *file* -> will checkout the conflicted file with all 3 relevant versions
# git reset --hard new-tip-commit  -> to move a branch

# core variables
[core]
  ; Don't trust file modes
  filemode = false
	editor = hx
	excludesfile = ~/.config/git/global_ignore
[init]
  defaultBranch = main
[commit]
  template = ~/.gitmessage

[alias]
  sshow = "!f() { git stash show stash^{/$*} -p; }; f"
  sapply = "!f() { git stash apply stash^{/$*}; }; f"
  sforce = "!git stash show -p | git apply && git stash drop"
  swd = "stash save --keep-index -u"
  ss = "stash push -m"
  sa = "stash apply stash^{/$1}"

  # INFO
  s  = "status"
  sl = "shortlog"
  l = "log --oneline --decorate --graph --all --relative-date"
  ll = "log --graph --pretty=format:'%C(yellow)%h%Creset -%C(magenta)%d%Creset %s %Cgreen(%cr) %C(bold blue)<%an>%Creset' --abbrev-commit --all"

  # COMITTING
  cm   = "commit -m"
  cma  = "commit --amend"

  # ADDING
  aa = "add ."
  ac = "add -u" # Only add changes, not untracked files

  # CHECKOUT / BRANCHING
  co = "checkout"
  nb = "checkout -b" # new branch
  db = "branch -d" # delete local branch
  drb = "push origin --delete" # delete remote branch
  dbb = !git branch -d $1 && git push origin --delete $1 && :
  b  = "branch" # show local branches
  ba = "branch --all" # show all branches
  # Pushes current branch to be remote tracked
  pbr = "!git rev-parse --abbrev-ref HEAD | while read out; do git push -u origin $out; done"
  # pbr = "push -u"
  drb = "push origin --delete"

  # PUSHING
  po = "push origin"
  pom = "push origin main"

  # DIFFING
  theirs = "checkout --theirs"
  ours   = "checkout --ours"
  d      = "diff"
  dc     = "diff --cached"
  d3     = "checkout --conflict=diff3"

  # MISC
  rhh = "reset --hard HEAD"
  ltbc = "commit -m \"Let there be code\""
  stash = "stash -u"
  i = "!git init && git add . && git status"
  oops = "reset --soft HEAD~"
  pc = diff --cached --diff-algorithm=minimal -w
  lb = !git reflog show --pretty=format:'%gs ~ %gd' --date=relative | grep 'checkout:' | grep -oE '[^ ]+ ~ .*' | awk -F~ '!seen[$1]++' | head -n 10 | awk -F' ~ HEAD@{' '{printf(\"  \\033[33m%s: \\033[37m %s\\033[0m\\n\", substr($2, 1, length($2)-1), $1)}'
  rs = "restore --staged"

  # CONFLICTS
  mine = "checkout --ours"
  theirs = "checkout --theirs"

[color]
  ui = auto

[difftool "sourcetree"]
	cmd = opendiff \"$LOCAL\" \"$REMOTE\"
	path =
[mergetool "sourcetree"]
	cmd = /Applications/SourceTree.app/Contents/Resources/opendiff-w.sh \"$LOCAL\" \"$REMOTE\" -ancestor \"$BASE\" -merge \"$MERGED\"
	trustExitCode = true

[pull]
	ff = only


# Per project git config
[includeIf "gitdir:~/.config/"]
  path = ~/.config/.gitconfig

[includeIf "gitdir:~/scratch/"]
  path = ~/.config/.gitconfig

[includeIf "gitdir:~/freewave/"]
  path = ~/freewave/.gitconfig



# From delta https://github.com/dandavison/delta
[core]
    pager = delta

[interactive]
    diffFilter = delta --color-only

[delta]
    navigate = true    # use n and N to move between diff sections
    light = false      # set to true if you're in a terminal w/ a light background color (e.g. the default macOS terminal)
	line-numbers = true

[diff]
    colorMoved = default
