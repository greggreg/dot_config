##################
#
# P = ctrl + b
#
# Session:
#   rename: P $
#   select: P s
#   new: P :new -s <name>
#   kill session: tmux kill-ses -t mysession
#   kill all but current: tmux kill-session -a
#   list: tmux ls
#   attach: tmux a
#   attach named: tmux a -t <name>
#   detach: tmux det
#   previous: P (
#   next: P )
#
# Window:
#   rename: P ,
#   select: P w
#   new: P c
#   go-to next: P n
#   go-to prev: P p
#   go-to num: P 0-9
#   kill: P &
#
# Pane:
#   split h: P %
#   split v: P "
#   kill : P x
#   show num: P q
#   go-to by num: P q 1-9
#   convert to window: P !
#   *move : P hkjl
#   *resize: P HKJL
#
# Config:
#   *reload: P r
#
##################

#-----GENERAL-------------------------------------------

# Proper screen emulation and colors
set -g default-terminal "xterm-kitty"

# Delay between escape and next char
set -sg escape-time 0

# Mouse mode
set -g mouse on
# I don't know what this was for
#bind -n WheelUpPane if-shell -F -t = "#{mouse_any_flag}" "send-keys -M" "if -Ft= '#{pane_in_mode}' 'send-keys -M' 'copy-mode -e; send-keys -M'"

# Don't cycle windows by scrolling the status bar
unbind-key -T root WheelUpStatus
unbind-key -T root WheelDownStatus

# Force a reload of the config file with P R
bind R source-file ~/.config/tmux/tmux.conf \; display-message 'tmux.conf reloaded...'

# Start windows and panes at 1, not 0
set -g base-index 1
setw -g pane-base-index 1

# Disable automatic window rename
setw -g automatic-rename off

# Expect UTF-8
setw -q -g utf8 on

# activity
set -g monitor-activity on
set -g visual-activity off


#-----NAVIGATION----------------------------------------

# pane navigation
bind -r h select-pane -L  # move left
bind -r j select-pane -D  # move down
bind -r k select-pane -U  # move up
bind -r l select-pane -R  # move right

# pane resizing
bind -r H resize-pane -L 2
bind -r J resize-pane -D 2
bind -r K resize-pane -U 2
bind -r L resize-pane -R 2


#-----LOOK&FEEL-----------------------------------------

set-option -g status-position bottom

# default statusbar colors
set-option -g status-style fg=colour136,bg=colour235 #yellow and base02

# default window title colors
set-window-option -g window-status-style fg=colour244,bg=default #base0 and default
#set-window-option -g window-status-style dim

# active window title colors
set-window-option -g window-status-current-style fg=colour166,bg=default #orange and default
#set-window-option -g window-status-current-style bright

# pane border
set-option -g pane-border-style fg=colour235 #base02
set-option -g pane-active-border-style fg=colour240 #base01

# message text
set-option -g message-style fg=colour166,bg=colour235 #orange and base02

# pane number display
set-option -g display-panes-active-colour colour33 #blue
set-option -g display-panes-colour colour166 #orange

# clock
set-window-option -g clock-mode-colour colour64 #green

# bell
set-window-option -g window-status-bell-style fg=colour235,bg=colour160 #base02, red


set -g status-interval 5               # set update frequencey (default 15 seconds)


set-option -g status-left-length 60
set -g status-left '#[fg=colour232,bg=colour154] #S '
set -g status-right '#[fg=colour235,bg=colour235,nobold,nounderscore,noitalics]#[fg=colour121,bg=colour235] %r  %a  %Y #[fg=colour238,bg=colour235,nobold,nounderscore,noitalics]#[fg=colour222,bg=colour238] #H #[fg=colour154,bg=colour238,nobold,nounderscore,noitalics]#[fg=colour232,bg=colour154] #(rainbarf --battery --remaining --no-rgb) '
# set -g status-right '#[default] '
setw -g window-status-format '#[default] | #I  #W | '
setw -g window-status-current-format '#[fg=colour222,bg=colour238,bold] > #I  #W < '


